<?php


return [
    'allowed_hosts' => explode(',', env('SECURITY_ALLOWED_HOSTS', 'localhost,127.0.0.1'))
];
