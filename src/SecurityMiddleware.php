<?php

namespace FritsStegmann\Laravel\Security;

use Closure;
use Illuminate\Http\Response;
use function Response;

class SecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $host = $request->server('SERVER_NAME');
        $allowedHosts = collect((array)config('security.allowed_hosts'));

        if (!$allowedHosts->contains($host)) {
            return Response('', 400);
        }

        if (!$allowedHosts->contains('localhost') && !$allowedHosts->contains('127.0.0.1')) {
            if (config('app.debug') || config('app.env') != 'production') {
                return Response(
                    'localhost is not in the allowed hosts but the server is in development mode',
                    500
                );
            }
        }

        /** @var Response $response */
        $response = $next($request);

        if ($response->status() == 401) {
            sleep(2);
        }

        $response->header('X-XSS-Protection', '1; mode=block');
        $response->header('X-Frame-Options', 'SAMEORIGIN');

        return $response;
    }
}
